import { SET_HALLS, ACTIVE_HALL, PURCHASE_PLACE } from '../actions';

const intStart = {
    halls: [],
    activeBlock: {}
}

export const setHall = (state = intStart, action) => {
    switch (action.type) {
        case SET_HALLS:
            return {
                ...state,
                halls: action.payload.map(hall => {
                    let rows = [], places = [], count = 0

                    for (let i = 0; i < hall.place; i++) {
                        if (count < (hall.place / hall.row) - 1) {
                            places = [
                                ...places,
                                {
                                    price: 150,
                                    number: i + 1,
                                    purchase: false
                                }
                            ]
                            count++;
                        } else {
                            places = [
                                ...places,
                                {
                                    price: 150,
                                    number: i + 1,
                                    purchase: false
                                }
                            ]
                            rows = [
                                ...rows,
                                places
                            ]
                            places = []
                            count = 0;
                        }
                    }
                    return {
                        ...hall,
                        place: rows
                    }
                })
            }
        case ACTIVE_HALL: 
            return{
                ...state,
                activeBlock: {
                    ...action.payload
                }
            }

        case PURCHASE_PLACE: 
            return{
                ...state,
                activeBlock: {
                    ...action.payload
                }
            }

        default:
            return state;
    }
}

export default setHall;
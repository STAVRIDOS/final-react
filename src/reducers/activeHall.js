import { SET_HALLS } from '../actions';


export const setHall = (state = [], action) => {
    switch (action.type) {
        case SET_HALLS:
            return {
                ...state,
                halls: action.payload.map(hall => {
                    let rows = [], places = [], count = 0

                    for (let i = 0; i < hall.place; i++) {
                        if (count < (hall.place / hall.row) - 1) {
                            places = [
                                ...places,
                                {
                                    price: '150grn',
                                    number: i + 1,
                                    purchase: false
                                }
                            ]
                            count++;
                        } else {
                            places = [
                                ...places,
                                {
                                    price: '150grn',
                                    number: i + 1,
                                    purchase: false
                                }
                            ]
                            rows = [
                                ...rows,
                                places
                            ]
                            places = []
                            count = 0;
                        }
                    }
                    return {
                        ...hall,
                        place: rows
                    }
                })
            }

        default:
            return state;
    }
}

export default setHall;
export const SET_HALLS = 'SET_HALLS';
export const ACTIVE_HALL = 'ACTIVE_HALL';
export const PURCHASE_PLACE = 'PURCHASE_PLACE';


export const setHalls = (halls) =>(dispatch, getState)=>{
    dispatch({
        type: SET_HALLS,
        payload: halls
    })
}

export const activeHall = (hall, link) =>(dispatch)=>{
        let activeHall = () => {
        let result;
        hall.forEach(element => {
            if(element.link === link.path){
                if(JSON.parse(localStorage.getItem(element.name))){
                    result = JSON.parse(localStorage.getItem(element.name))
                }else{
                    localStorage.setItem(element.name, JSON.stringify({
                        ...element,
                        active: []
                    }));
                }

                result = JSON.parse(localStorage.getItem(element.name))
            }
        })
        return result;
    }

    dispatch({
        type: ACTIVE_HALL,
        payload:  activeHall()
    })
}

export const purchasePlace = (place) =>(dispatch)=>{
    let result;
    if(place.active.length !== 0){
        place.active.forEach(el=>{
            if(!place.place[el.row][el.place].purchase){
                place.place[el.row][el.place].purchase = true
                place.sum += el.price;

                result = {
                    ...place,
                    active: [...place.active],
                }
            }else{
                result = JSON.parse(localStorage.getItem(place.name))
            }
        })
    }else{
        place.place.forEach(row =>{
            row.forEach(place=>{
                place.purchase = false
            })
        })
        result = {
            ...place,
            sum: 0
        };
    }

    localStorage.setItem(place.name, JSON.stringify(result));

    dispatch({
        type: PURCHASE_PLACE,
        payload: localStorage.getItem(place.name) ? JSON.parse(localStorage.getItem(place.name)) : result
    })
}

import React, { Component } from 'react';
import { connect } from 'react-redux';

import { purchasePlace } from '../../actions'

export class Cell extends Component {
    activePlace = (e)=>{
        let newPlace = this.props.active.activeBlock,
            i = e.target.dataset
        
            this.props.purchasePlace({
                ...newPlace,
                active: [
                    ...newPlace.active,
                    {
                        row: +i.row,
                        place: +i.number,
                        price: +i.price
                    }
                ]
            })
        
    }

    render() {
        let { price, number, purchase } = this.props.places;
        return (
            <div 
                data-number={this.props.place} 
                data-price={price} 
                data-row={this.props.row}
                className={purchase ? 'place purchase' : 'place'} 
                onClick={this.activePlace}
                >
                {number}
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return ({
        active: state

    })
}

const mapDispatchToProps = (dispatch, getState) => {
    return ({
        purchasePlace: (halls) => {
            dispatch(purchasePlace(halls))
        }
    })
}
export default connect(mapStateToProps, mapDispatchToProps)(Cell);
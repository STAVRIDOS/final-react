import React from 'react';

import Cell  from './Cell'

export const Row = ({rows, row}) => {
    return (
        <div className='hall_table__hall'>
        {
            rows.map((element, index)=>(
                <Cell key={index} places={element} row={row} place={index}/>
            ))
        }
        </div>
    )
}

export default Row;

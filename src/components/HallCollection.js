import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';

export class HallCollection extends Component {

    render() {
        let { halls } = this.props;
        return (
            <div className='hall__row'>
                {
                    halls.map(hall => (
                        <Link key={hall.key} to={ hall.link } style={{ background: hall.color }} className='hall'>
                            {hall.name}
                        </Link>
                    ))
                }
            </div>
        )
    }
}

HallCollection.defaultProps = {
    halls: []
}


const mapStateToProps = (state, ownProps) => {
    return ({
        ...state
    })
}

export default connect(mapStateToProps, null)(HallCollection);

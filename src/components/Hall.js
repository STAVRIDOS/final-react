import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Row } from './HallConst';
import { activeHall, purchasePlace } from '../actions'

export class Hall extends Component {
    componentWillMount() {
        let { halls, match, activeHall } = this.props;
        activeHall(halls, match)
    }

    placeReset = name => () =>{
        let {purchasePlace, activeBlock } = this.props;
        purchasePlace({
            ...activeBlock,
            active: []
        })
    }

    render() {
        let { place , name, sum, active } = this.props.activeBlock;
        return (
            <div className='hall__block'>
                <div className ='hall_table__description'>
                    <h1>Вы выбрали:</h1>
                    <h2>Зал: {name}</h2>
                    <div>
                        {
                            active !== undefined &&
                            active.map((element, index) => (
                                <p key={index} className='selected_place'>
                                    <span className="places_info">Ряд: {element.row+1}, место:{element.place+1}</span>
                                    <span className="price_info">{element.price} грн</span>
                                </p>
                            ))
                        }
                    </div>
                    <div className="hall_table_bottom__block">
                        <div className="button__null" onClick={this.placeReset(name)}>Обнулить заказ</div>
                        <div className='hall_table__sum'>
                            <span className="title__sum">Всего: </span> 
                            <span className='result__sum'>{sum} грн</span>
                        </div>
                    </div>
                </div>
                <div className="test">
                    <div className='screen__preview'>
                        {
                            place !== undefined &&
                            place.map((row, index) => {
                                return (<Row key={index} rows={row} row={index} />)
                            })
                        }
                    </div>
                </div>
            </div>
        )
    }
}
    

const mapStateToProps = (state, ownProps) => {
    return ({
        ...state
    })
}

const mapDispatchToProps = (dispatch) => {
    return ({
        activeHall: (hall,link)=> {
                dispatch(activeHall (hall, link))
        },
        purchasePlace: (halls) => {
            dispatch(purchasePlace(halls))
        }
    })
} 


export default connect(mapStateToProps, mapDispatchToProps)(Hall);  
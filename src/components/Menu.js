import React from 'react'
import { Link } from 'react-router-dom'
export const Menu = () => {
    return (
        <div className='top__menu'>
            <Link to='/'>Главная</Link>
        </div>
    )
}

export default Menu

import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { connect } from 'react-redux';

import HallCollection from './HallCollection';
import Hall from './Hall'

import { setHalls } from '../actions'

import '../style/home.css'

export class Home extends Component {
    state = {
        halls: [
            {
                name: 'Красный Зал',
                row: 10,
                place: 150,
                color: 'red',
                link: '/red',
                key: 1
            },
            {
                name: 'Зеленый зал',
                row: 5,
                place: 100,
                color: 'green',
                link: '/green',
                key: 2
            },
            {
                name: 'Желтый зал',
                row: 2,
                place: 10,
                color: 'yellow',
                link: '/yellow',
                key: 3
            }
        ]
    }
    componentWillMount() {
        let { setHalls } = this.props;
        setHalls(this.state.halls)
    }
    render() {
        let { halls } = this.state;
        return (
            <div className='row_all__home'>
                    <Switch>
                        <Route exact path='/' render={props => (
                            <HallCollection />
                        )} />
                        {
                            halls.map((hall, index) => (
                                <Route key={index} exact path={hall.link} render={props => (
                                    <Hall {...props} hall={hall} test={hall.link} />
                                )} />
                            ))
                        }

                    </Switch>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return ({
        halls: state.halls

    })
}

const mapDispatchToProps = (dispatch, getState) => {
    return ({
        setHalls: (halls) => {
            dispatch(setHalls(halls))
        }
    })
}
export default connect(mapStateToProps, mapDispatchToProps)(Home);

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter} from 'react-router-dom'

import { Menu } from './components'
import Home from './components/Home'
import store from './redux/store';

export class App extends Component {

    render() {
        return (
            <Provider store={store}>
                 <BrowserRouter>
                    <div className="row_all">
                        <Menu />
                        <Home />
                    </div>
                </BrowserRouter>
            </Provider>
        )
    }
}

export default App

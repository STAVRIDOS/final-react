import React, { Component } from 'react';
import { BrowserRouter, NavLink, Switch, Route, Link } from 'react-router-dom';

import { Home, Posts, Post } from './Components/Pages'
import Loader from './Components/Loader';

import './Components/Style/App.css'
import logo from './img/logo.png'

export class App extends Component {

    state = {
        loader: true,
        data: []
    }

    componentDidMount() {
        fetch('https://jsonplaceholder.typicode.com/posts')
            .then(res => (
                res.json()
            )).then(data => {
                this.setState({
                    loader: false,
                    data: data
                })
            })
    }
    render() {

        let { loader, data } = this.state;
        return (
            <>
                <header className='row_navigation'>
                    <div className="logo_img">
                        <Link to="/">
                            <img src={logo} alt={logo} />
                        </Link>
                    </div>
                    <div className="navigation">
                        <NavLink exact className='button_navigation' to='/'>
                            <span>Главная</span>
                        </NavLink>
                        <NavLink className='button_navigation' to='/posts'>
                            <span>Новости</span>
                        </NavLink>
                    </div>
                </header>
                {
                    loader ?
                        <Loader /> :
                        <>
                            <Switch data={data}>
                                <Route exact path='/' render={props => (
                                    <Home {...props} data={data} />
                                )} />
                                <Route exact path='/posts' render={props => (
                                    <Posts {...props} data={data} />
                                )} />
                                <Route exact path='/posts/:id' render={props => (
                                    <Post {...props} data={data} />
                                )} />
                            </Switch>
                        </>
                }
            </>
        )
    }
}

export default App;
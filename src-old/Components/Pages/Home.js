import React, { Component } from 'react'
import { Link } from 'react-router-dom'


export class Home extends Component {

    state = {
        data: this.props.data,
        count: 20,
        step: 20,
        addButton: true,
        remButton: false
    }


    postAdd = () => {
        let { step, data, count } = this.state;
        this.setState({
            count: count + step,
            addButton: data.length > (count + step) ? true : false,
            remButton: (count + step > step) && true
        })
    }

    postRemove = () => {
        let { step, data, count } = this.state;
        this.setState({
            count: count - step,
            addButton: data.length > count - step ? true : false,
            remButton: (count - step) === step ? false : true
        })
    }

    render() {

        let { data, count, remButton, addButton } = this.state;
        console.log(this.props.children)
        return (
            <div>
                {
                    data.map((post, index) => {
                        if (index < count) {
                            return (
                                <div className="posts_block">
                                    <Link to={`/posts/${post.id}`} >{index + 1} - {post.title}</Link>
                                </div>
                            )
                        }
                    })
                }
                <div>
                    { addButton && <button className="button_home" onClick={this.postAdd}>Показать еще</button> }
                    { remButton && <button className="button_home" onClick={this.postRemove}>Убрать последние</button> }
                </div>
            </div>
        )
    }
}

export default Home;

import React, { Component } from 'react';
import { Link } from 'react-router-dom';


export class Posts extends Component {
    render() {

        let {data, match} = this.props;

        
        return (
            <>  
                <ul>
                    {
                        data.map(el => (
                            <li>
                                <Link to={`${match.path}/${el.id}`} >{el.title}</Link>
                            </li>
                        ))
                    }
                </ul>
            </>
        )
    }
}

export default Posts

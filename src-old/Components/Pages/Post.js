import React, { Component } from 'react'

export class Post extends Component {
    state = {
        users: [],
        loaded: false
    }

    visualCommetns = () =>{
        console.log(this.props.match.params.id)
        console.log(this)
        fetch(`https://jsonplaceholder.typicode.com/posts/${this.props.match.params.id}/comments`)
            .then(res => (
                res.json()
            )).then(data => {

                this.setState({
                    users: data,
                    loaded: true
                })
            })
    }

    render() {

        let { data, match } = this.props;
        let { users, loaded } = this.state;

        return (
            <div className='posts_block'>
                <div>
                    {
                        data.map(el => {
                            if (el.id === +match.params.id) {
                                console.log('id', el.id)
                                console.log('userId', el.userId)
                                return (
                                    <div>
                                        <h1>{el.title}</h1>
                                        <div className='news_text'>{el.body}</div>
                                    </div>
                                )
                            }
                        })
                    }
                </div>
                <div className="row_comment">
                <div className="title_comments">
                    <h2>Комментарии:</h2>
                    <span>количество: {users.length}</span>
                </div>
                    {
                        loaded ? 
                        users.map(user => (
                            <div className="comments_user">
                                {user.body}
                                <div className="user">
                                    <span>Имя:</span> 
                                    {user.name}
                                </div>
                                <div className="mail">
                                    <span>Email:</span> 
                                    {user.email}
                                </div>
                                <div className="comments">
                                    <span>Комментарий:</span> 
                                    {user.body}
                                </div>
                            </div>
                        )) :
                        <button onClick = {this.visualCommetns }>Показать комментарии</button>
                    }
                </div>
            </div>
        )
    }
}

export default Post;
